package com.example.cuacaku;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.cuacaku.Model.Data;
import com.example.cuacaku.Model.Weather;

import java.util.Date;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

public class MainActivity extends AppCompatActivity {
    Data data;
    List<Weather> weather;
    TextView tvCuaca, tvIcon;
    ImageView imgIcon;
    Retrofit retrofit;
    String icon = "";
    String cuaca = "";
    String ico = "";
    String BASE_URL = "https://samples.openweathermap.org/";
    String imgUrl = "";
    int id;
    int shortId;
    int lat;
    int lon;
    long sunrise;
    long sunset;
    Typeface tf ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupView();
        setupNetwork();
    }

    private void setupNetwork() {
        lat = 35;
        lon = 139;
        Service service = getRetrofit().create(Service.class);
        service.getData(
                "https://samples.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&appid=b6907d289e10d714a6e88b30761fae22")
                .enqueue(new Callback<Data>() {
                    @Override
                    public void onResponse(Call<Data> call, Response<Data> response) {
                        data = response.body();
                        weather = data.getWeather();
                        cuaca = weather.get(0).getDescription();
                        ico = /*weather.get(0).getIcon()*/"13d";
                        tf = Typeface.createFromAsset(getBaseContext().getAssets(), "font/weather.ttf");
                        sunrise = data.getSys().getSunrise();
                        sunset = data.getSys().getSunset();
                        id = /*weather.get(0).getId()*/600;
                        shortId = /*weather.get(0).getId()*/600/100;
                        imgUrl = "http://openweathermap.org/img/wn/"+ico+"@2x.png";
                        Glide.with(getApplicationContext()).load(imgUrl).into(imgIcon);
                        setIconWeather(id, sunrise,sunset);
                        tvIcon.setTypeface(tf);
                        tvIcon.setText(icon);
                        tvCuaca.setText(cuaca);
                    }

                    @Override
                    public void onFailure(Call<Data> call, Throwable t) {

                    }
                });
    }

    private Retrofit getRetrofit() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        if (retrofit == null){
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }

    public interface Service{
        @GET
        Call<Data> getData(@Url String url);
    }

    private void setupView() {
        tvIcon = findViewById(R.id.tv_icon);
        tvCuaca = findViewById(R.id.tv_cuaca);
        imgIcon = findViewById(R.id.img_icon);

    }

    private String setIconWeather(int getId, long getSunrise, long getSunset){
        if (getId == 800){
            long currentTime = new Date().getTime();
            if (currentTime >= getSunrise && currentTime < getSunset){
                icon = getString(R.string.weather_sunny);
            }else {
                icon = getString(R.string.weather_clear_night);
            }
        }else {
            switch (shortId){
                case 2 : icon = getString(R.string.weather_thunder);
                    break;
                case 3 : icon = getString(R.string.weather_drizzle);
                    break;
                case 7 : icon = getString(R.string.weather_foggy);
                    break;
                case 8 : icon = getString(R.string.weather_cloudy);
                    break;
                case 6 : icon = getString(R.string.weather_snowy);
                    break;
                case 5 : icon = getString(R.string.weather_rainy);
                    break;
            }
        }
        return icon;
    }

}
